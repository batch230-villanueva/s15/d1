// import hehe from './hehe';

console.log('Hello World!')

// Statements in programming are instructions that we tell the computer to perform
// JS statements usually end with semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
// A syntax in programming, it is the set of rules that describes how statements must be constructed.
// All lines/blocks of code should be written in a specific manner/structure and sequence to work.


console.log("------------");
console.log(">> VARIABLES");

let myVariable;

let message = "Congrats B230 for completing Capstone 1!";

console.log(myVariable); // returns undefined, unassigned variable
console.log(message); // returns the string assigned to it

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

*/

let messageNew = "Hooray! I finished Capstone 1";
console.log(messageNew);

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

console.log("------------------");
console.log('>> Reassigning variable values');

productName = "Laptop";
console.log(productName);

let supplier;

supplier = 'John Smith Tradings';
console.log(supplier);

// Reassignment

supplier = "Zuitt Store";
console.log(supplier);

s = "hehe";
console.log(s);
var s;

console.log("-------------------");
console.log(">> Multiple Variable Declarations");

let x,y;
x=1;
y=2;
console.log(x,y); // BAD PRACTICE

let productCode = " DC017";
let productBrand = "Dell";
console.log(productCode, productBrand);

console.log(x + y);

// Data Types

console.log("-------------------");
console.log("Data Types");

// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines';
let province = "Cavite";

// concatenating strings
let fullAddress = province + ', ' + country;
console.log(fullAddress);

// The escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between texk.

let mailAddress = "Metro Manila\nPhilippines";

console.log(mailAddress);

let updateMessage = "John's Employees went home early.";
console.log(updateMessage);
updateMessage = 'Johns employee\'s went home early';
console.log(updateMessage);

// Numbers
console.log("------------");
console.log(">> [Data Type] Numbers ");

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

// console.log(hehe + grade);

// Bool data type

let isMarried = true;
let isRich = false;

console.log(isMarried ? "I am married." : "I am happy.");
console.log(isRich ? "I am rich." : "I am not rich.");

// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

console.log("------------");
console.log(">> Arrays ");

// let/const arrayName = [elementA, elementB, elementC]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// diff data types

let details = ['John', 'Smith', 32, true];
console.log(details);

// Objects
// Used to create complex data that contains pieces of info that are relvant to each other
// Python Equivalent: Dictionary

// Structure:
// let/const objectName = {
//     property1: value1,
//     property2: value2,
//     property3: value3,
// }

console.log("-----------------------");
console.log(">> [Data Type] Objects");

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["+63917 123 4567", "8123 4567"],
    address: {
        housenumber: '345',
        city: 'Manila',

    }
}

console.log(person);

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6,
}

console.log(myGrades);

// typeof operator is used to determine type of variable

console.log(typeof myGrades);

console.log(typeof grades);

console.log("--------------");
console.log(">> Constant Objects and Arrays");

//index starts with zero
const anime = ["One Piece", "Kimetsu no Yaiba", "Dragon Ball", "SpyXFamily"];

console.log(anime);
console.log(anime[0]);

anime[0] = "Bleach";
console.log(anime);

console.log("---------------------");
console.log(">> Null and undefined ");
// Null
// used to express absence of a value in variable

let spouse = null;

console.log(spouse);

let myNumber = 0; // null is its own data type compared to 0 or an empty string
let myString ="";

let fullName;
console.log(fullName);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing

